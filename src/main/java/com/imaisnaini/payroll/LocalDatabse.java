package com.imaisnaini.payroll;

import com.imaisnaini.payroll.entity.Employee;
import com.imaisnaini.payroll.entity.Order;
import com.imaisnaini.payroll.repository.EmployeeRepository;
import com.imaisnaini.payroll.repository.OrderRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class LocalDatabse {
    public static final Logger log = LoggerFactory.getLogger(LocalDatabse.class);

    @Bean
    CommandLineRunner initDatabase(EmployeeRepository employeeRepository, OrderRepository orderRepository){
        return args -> {
            employeeRepository.save(new Employee("Mark", "Zuberk", "CTO"));
            employeeRepository.save(new Employee("Liam", "Anderson", "HRGA"));

            employeeRepository.findAll().forEach(employee -> log.info("Preloading " + employee));

            orderRepository.save(new Order("MacBook Pro", Status.COMPLETED));
            orderRepository.save(new Order("Nexus 5s", Status.IN_PROGRESS));

            orderRepository.findAll().forEach(order -> log.info("Preloading " + order));

        };
    }
}
