package com.imaisnaini.payroll.repository;

import com.imaisnaini.payroll.entity.Order;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderRepository extends JpaRepository<Order, Long> {
}
