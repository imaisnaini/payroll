package com.imaisnaini.payroll;

public enum Status {
    IN_PROGRESS,
    COMPLETED,
    CANCELLED
}
